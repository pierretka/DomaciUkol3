﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SU_05
{
    class SU_05
    {
        static void Main(string[] args)
        {
            Random hodKostkou = new Random();
            int kostka = 0;

            while (kostka != 6)
            {
                kostka = hodKostkou.Next(1, 7);
                Console.WriteLine("Na kostce padlo " + kostka);
            }
            Console.WriteLine("Na kostce padlo šest.");
            Console.ReadLine();  
        }
    }
}
