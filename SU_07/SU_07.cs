﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SU_07
{
    class SU_07
    {
        static void Main(string[] args)
        {
            for (int a = 0; a < 10; a++)
            {
                for (int b = 0; b < 5; b++)
                {
                    if (a%2 != 0)
                    {
                        Console.Write("#");
                        Console.Write("@");
                    }
                    else
                    {
                        Console.Write("@");
                        Console.Write("#");
                    }
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}